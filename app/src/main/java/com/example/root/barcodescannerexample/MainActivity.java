package com.example.root.barcodescannerexample;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bim.io.OnDataReceive;
import com.bimsdk.bluetooth.BluetoothConnect;

public class MainActivity extends AppCompatActivity {

    TextView tvStaticBarcode;
    EditText tvBarCode;
    String barcodeText;
    Button btPlay;
    //private MediaPlayer mediaPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BluetoothConnect.BindService(this);
        initView();
        getData();
        getCompare();


    }


    private void initView() {
        tvBarCode = (EditText) findViewById(R.id.tvBarCode);
        barcodeText = tvBarCode.getText().toString().trim();
        btPlay = (Button) findViewById(R.id.btPlay);

        Log.d("*****@@@", "" + barcodeText);
        tvStaticBarcode = (TextView) findViewById(R.id.tvStaticBarcode);
        tvStaticBarcode.setText("2255195000000010043981");
        // mediaPlayer = MediaPlayer.create(this, R.raw.song);

       /* btPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.start();
                Toast.makeText(getApplicationContext(), "Playing sound", Toast.LENGTH_SHORT).show();
            }
        });*/

    }

    private void getData() {

        BluetoothConnect.SetOnDataReceive(new OnDataReceive() {

            @Override
            public void DataReceive(byte[] InfoByte) {

                tvBarCode.append(new String(InfoByte) + "\n");

            }
        });

    }

    private void getCompare() {
        tvBarCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                barcodeText = tvBarCode.getText().toString().trim();
                String staticBarcode = tvStaticBarcode.getText().toString().trim();

                Log.d("*****###", "" + barcodeText);
                Log.d("*****123", "" + staticBarcode);

                if (staticBarcode.equals(barcodeText)) {

                    Toast.makeText(MainActivity.this, "Code Matched", Toast.LENGTH_SHORT).show();


                } else {
                    Toast.makeText(MainActivity.this, "Not Matched", Toast.LENGTH_LONG).show();
                }
                return true;

            }
        });


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        BluetoothConnect.unBindService(this);
    }
}
